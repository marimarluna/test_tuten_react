import axios from 'axios';

export function authenticate(email, password) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'PUT',
      url: `https://dev.tuten.cl/TutenREST/rest/user/${email}`,
      headers: {
        'Content-Type': 'application/json',
        app: 'APP_BCK',
        password: password,
      },
    })
      .then(response => resolve(response.data))
      .catch((error) => reject(error));
  });
}

export function listBooking(adminemail, token){
  return new Promise((resolve, reject) => {
    axios({
      method: 'GET',
      url: 'https://dev.tuten.cl/TutenREST/rest/user/contacto%40tuten.cl/bookings?current=true',
      headers: {
        'Content-Type': 'application/json',
        app: 'APP_BCK',
        adminemail: adminemail,
        token: token,
      }
    })
    .then(response => resolve(response.data))
    .catch(error => reject(error));
  });
}