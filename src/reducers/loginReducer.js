export const LOGIN = 'loginReducer/LOGIN';

const initialState = {
  email: null,
  token: null,
}

export const login = (email, token) => (dispatch) => {
    dispatch({
      type: LOGIN, email, token,
    });
  };

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        email: action.email,
        token: action.token
      };
    default:
      return state;
  }
}