import React, {Component} from 'react';
import { authenticate } from '../../api/mockApi';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { login } from '../../reducers/loginReducer';


class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          username: '',
          password: ''
        }
      }
    
      onChangeUser = (e) => {
        this.setState({ ...this.state, username: e.target.value });
      }
    
      onChangePassword = (e) => {
        this.setState({ ...this.state, password: e.target.value });
      }
      
      onSubmit = (e) => {
        const {username, password } = this.state;
        const { doLogin } = this.props;
        e.preventDefault();
        authenticate(username, password)
        .then((user) => {
            console.log(user.sessionTokenBck);
            doLogin(user.email, user.sessionTokenBck);
            browserHistory.push('/hello');
        })
        .catch((err) => {
            console.log(err);
        })
      }
    render() {

        return (
        <div>
            <h1>Login</h1>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label className="control-label">Username</label>
                    <input className="form-control" type="text" onChange={this.onChangeUser} required />
                </div>
                <div className="form-group">
                    <label className="control-label">Password</label>
                    <input className="form-control" type="password" onChange={this.onChangePassword} required />
                </div>
                <div>
                    <input className="btn btn-primary btn-lg" type="submit" onClick={this.onSubmit} />
                </div>
            </form>
        </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
  });
  const mapDispatchToProps = dispatch => ({
    doLogin: (email, token) => dispatch(login(email, token)),

  });
  
export default connect(mapStateToProps, mapDispatchToProps )(LoginPage);

