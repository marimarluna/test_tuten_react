import React, {Component} from 'react';
import { connect } from 'react-redux';
import { listBooking } from '../../api/mockApi';
import Griddle, {plugins, RowDefinition, ColumnDefinition } from 'griddle-react';

const NewLayout = ({ Table, Pagination, Filter }) => (
  <div>
    <Filter />
    <Table />
    <Pagination />
  </div>
);

class HelloPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listBooks: [],
    }
  }

  componentWillMount(){
    const { user } = this.props;
    console.log(user);
    listBooking(user.email, user.token)
        .then((list) => {
            this.setState({listBooks: this.normalizeItems(list)})
        })
        .catch((err) => {
            console.log(err);
        })
  }

  normalizeItems(results) {
    const resultsNormalize = results.map((r,k) => ({
      BookingId: r.bookingId,
      Cliente: `${r.tutenUserClient.firstName || ''} ${r.tutenUserClient.lastName || ''}`,
      "Fecha de Creacion": new Date(r.bookingTime).toDateString(),
      Direccion: r.locationId.streetAddress,
      Precio: r.bookingPrice,
    }));

    return resultsNormalize;
  }

  render() {
    const { listBooks } = this.state;
    console.log(listBooks)  
    return (
      <div style={{marginTop: '40px'}}>
        <Griddle data={listBooks} plugins={[plugins.LocalPlugin]} components={{ Layout: NewLayout }} >
          <RowDefinition>
            <ColumnDefinition id="BookingId" title="BookingId"/>
            <ColumnDefinition id="Cliente" title="Cliente"/>
            <ColumnDefinition id="Fecha de Creacion" title="Fecha de Creación"/>
            <ColumnDefinition id="Direccion" title="Direccion"/>
            <ColumnDefinition id="Precio" title="Precio"/>
          </RowDefinition>
        </Griddle>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.loginReducer,
});

export default connect(mapStateToProps)(HelloPage);
