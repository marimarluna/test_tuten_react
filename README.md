This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# TEST TUTEN EN REACTJS

## Dependencias utilizadas: 

* axios v-0.19.0: para la conexión al api rest
* boostrap v-3.3.7: para dar un poco de diseño al login form
* griddle-react v-1.13.1: es una dependencia para creación de grilla.
* react-router v-3.0.0: para la navegación entre componentes.
* react-redux v-4.4.5: para mantener los datos del usuario entre componentes

## Instrucciones para replicar:

`git clone https://marimarluna@bitbucket.org/marimarluna/test_tuten_react.git`

`cd test_tuten`

`npm install`

`npm start`

La app abrirá en modo de desarrollo.<br>

Abrir [http://localhost:3000](http://localhost:3000) para visualizar en el navegador
